**This repository build for learning django framework**

---


## you have to install python3 on your machine and virtualenv
1. sudo apt install python3
2. sudo apt install python3-pip
3. sudo apt install virtualenv

## setup your env
1. virtualenv .pythonenv

## to activate your env
1. source .pythonenv/bin/activate

